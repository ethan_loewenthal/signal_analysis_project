function strJson = toJson(utObj,waveObjs,num_sigs)
%TOJSON Returns a string in json format of the properties of this object.

isSignalFail = false;

for i = [1:num_sigs]
    
    signalResultMessage = '';
    
    if conditional(waveObjs(i).status , true , false)
        isSignalFail = true;
        if conditional(waveObjs(i).clipped , true , false)
            signalResultMessage = 'clipping';
        elseif conditional(waveObjs(i).noiseratioLow , true , false)
            signalResultMessage = 'noise_ratio_low';
        elseif conditional(waveObjs(i).tofBad , true , false)
            if conditional(waveObjs(i).boltFatigued , true , false)
                signalResultMessage = 'bolt_fatigued';
            else
                signalResultMessage = 'ToF_bad';
            end
        elseif conditional(waveObjs(i).maxminSep , true , false)
            signalResultMessage = 'min_max_separation';
        else
            signalResultMessage = 'bad_data';
        end
    end
    
    sProps_sig = struct(...
        'type', conditional(waveObjs(i).waveType == 'L', 'L', conditional(waveObjs(i).waveType == 'S', 'S', 'U')),...
        'status', conditional(waveObjs(i).status , 'bad' , 'good'), ...
        'reason', signalResultMessage);%, ...
    %         'rawData',       waveObjs(i).rawData);
    
    signal_output(i) = sProps_sig;
end

groupResultMessage = '';

if conditional(utObj.status || isSignalFail, true , false)
    if conditional(utObj.utBad , true , false)
        groupResultMessage = 'ratio_bad';
    elseif isSignalFail
        groupResultMessage = 'signal_fail';
    else
        groupResultMessage = 'bad_data';
    end
end

sProps_group = struct('avgLongTOF', utObj.avgtofL, ...
    'avgShearTOF',   utObj.avgtofS, ...
    'ratio',         utObj.avgutResponse, ...
    'status',        conditional(utObj.status , 'bad' , 'good'), ...
    'reason',        groupResultMessage,...
    'signalResults', signal_output);%...

strJson = jsonencode(sProps_group);

end