function [strJson, utObj, waveObjs] = analyzeWaves(data,boltLength,wave_type,tension,sig_num)

% This function runs WaveAnalysis to analyze signal and calculate ToF
% values, and includes checks of the signal itself, as well as checks of
% the ToF values. Then, it runs WaveGroup to match data from multiple S and
% L waves wave signals, calculate average ToFs, and calculate UT responses,
% and includes checks of the UT responses.

num_sigs = size(data,1); % get # of signals
waveObjs = WaveAnalysis.empty; % set up empty object
ties = []; % keep track of analyses that have a group-choosing tie

% Iterate through all the files in the directory and analyze the waves
for i = 1:num_sigs
    
    waveObjs(i) = WaveAnalysis(data{i},boltLength,wave_type{i},tension(i),sig_num(i),[]); % analyze the data

    if waveObjs(i).groupTie == 1
        ties = [ties i];
    end
    
end

% If there are ties in choosing groups, get the ToF from a sibling signal
% to break the tie and re-run that analysis, overwriting the old analysis
if ~isempty(ties)
    for j = ties
        
        tof_avg_sibling = getsiblingTof(waveObjs,num_sigs,j);
        
        waveObjs(j) = WaveAnalysis(data{j},boltLength,wave_type{j},tension(j),sig_num(j),tof_avg_sibling);
    
    end
end

utObj = WaveGroup(waveObjs,num_sigs); % match results by tension and signal # and calculate UT responses
strJson = toJson(utObj,waveObjs,num_sigs); % write relevant output to json format

end