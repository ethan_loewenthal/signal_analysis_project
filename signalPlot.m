function signalPlot(waveObjs)

% Plots raw signal and marks maxs & mins (overall echo and in-phase)

data        = waveObjs.rawData;
time        = waveObjs.Time;
tension     = waveObjs.boltTension;
num_echo    = waveObjs.numberofEchos;
res         = waveObjs.peakTimes;
echo_peaks  = waveObjs.peakAmplitudes;
loc_max_ind = waveObjs.localmaxIndices;
loc_min_ind = waveObjs.localminIndices;

% create figure and clear old figure, and plot raw data
figure;
clf;
plot(time,data,'b');
hold on;
xlabel('time ({\mu}s)');
ylabel('amplitude');
title(strcat(['Tension = ',num2str(tension)]));

for i=1:num_echo
    % plot max and min of this echo and label
    plot(res(1,i)         ,echo_peaks(1,i         ),'or');
    plot(res(1,i+num_echo),echo_peaks(1,i+num_echo),'ok');
    text(res(1,i)         ,echo_peaks(1,i    )+0.01,'max',...
        'HorizontalAlignment','center','VerticalAlignment','bottom');
    text(res(1,i+num_echo),echo_peaks(1,i+num_echo)-0.01,'min',...
        'HorizontalAlignment','center','VerticalAlignment','top');
    
    % plot local maxs & mins w/ labels
    phasepi = loc_max_ind{i};
    phaseni = loc_min_ind{i};
    
    if ~any(isnan(phasepi)) && ~any(isnan(phaseni))
        tofpi = time(phasepi);
        tofni = time(phaseni);
        
        plot(tofpi,data(phasepi),'r<');
        plot(tofni,data(phaseni),'k<');
        
        loc_peaksp = data(phasepi);
        for loci = 1:numel(tofpi)
            if rem(loci,2) == 1
                text(tofpi(loci)-10,loc_peaksp(loci),...
                    num2str(loci),'HorizontalAlignment','right','VerticalAlignment','middle')
            else
                text(tofpi(loci)-20,loc_peaksp(loci),...
                    num2str(loci),'HorizontalAlignment','right','VerticalAlignment','middle')
            end
        end
        loc_peaksn = data(phaseni);
        for loci = 1:numel(tofni)
            if rem(loci,2) == 1
                text(tofni(loci)-10,loc_peaksn(loci),...
                    num2str(loci),'HorizontalAlignment','right','VerticalAlignment','middle');
            else
                text(tofni(loci)-20,loc_peaksn(loci),...
                    num2str(loci),'HorizontalAlignment','right','VerticalAlignment','middle');
            end
        end
    end
end
end
