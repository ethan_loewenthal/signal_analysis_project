function [peak_to_noise_ratio, out] = noiseRatioCheck(this)

out = 0;

noise_pos  = this.signalNoisepos;
noise_neg  = this.signalNoiseneg;
echo_peaks = this.peakAmplitudes;
min_ratio  = this.minnoiseRatio;
num_echos  = this.numberofEchos;

echo_ind_pos = [1:num_echos];
echo_ind_neg = [num_echos+1:2*num_echos];

% get min pos & neg peaks
peak_pos = echo_peaks(echo_ind_pos);
peak_neg = abs(echo_peaks(echo_ind_neg));

% get min peak-to-noise ratio peaks to positive noise
ratio_pos = peak_pos./noise_pos;
ratio_neg = peak_neg./noise_neg;
peak_to_noise_ratio = [ratio_pos ratio_neg];

if any(peak_to_noise_ratio < min_ratio)
    out = 1;
end

end