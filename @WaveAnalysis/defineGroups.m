% Defines unique groups.
%
% INPUT PARAMETERS
% this: the WaveAnalysis object.
%
% OUTPUT PARAMETERS
% groupIndices: a matrix of 0's or 1's where each row is one unique group
%	and each column is 1 corresponding to the indices of the ToF array.
function this = defineGroups(this)

tof_sort = this.tofAscending;
nToFs    = numel(tof_sort);

group_indices = zeros(1,nToFs); % find groups using bottom up method

for i = 1:nToFs
    maxF2InGroup = tof_sort(i) + this.groupWindow;
    for j = i:nToFs
        if tof_sort(j) < maxF2InGroup
            group_indices(i,j) = 1;
        end
    end
end

% Get rid of redundant groups
if nToFs > 1
    for i = nToFs:-1:2
        if sum(group_indices(i-1,:)) == sum(group_indices(i,:)) + 1
            group_indices(i,:) = [];
        end
    end
end

% create matrix of all groups
tof_groups                     = nan*group_indices;
tof_sort_mat                   = tof_sort(ones(size(group_indices,1),1),:);
tof_groups(group_indices == 1) = tof_sort_mat(group_indices == 1);

% attach results to the object
this.groupIndices = group_indices;
this.tofGroups    = tof_groups;

end