function [out, out_gg] = checkClipping(this)

out    = 0;

data   = abs(this.rawData);
out_gg = double.empty;

for gage = 1:size(data,1)
    if any(data(gage,:) > this.clippingAmp)
        out = 1;
        out_gg = [out_gg, gage];
    end
end
