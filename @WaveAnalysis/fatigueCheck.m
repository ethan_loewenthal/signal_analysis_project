function [echo_prematurity, out] = fatigueCheck(this)

out = 0;

tof              = this.timesofFlight;
% tof_average      = this.averageTof;
res              = this.peakTimes;
wave_type        = num2str(this.waveType);
num_echo         = this.numberofEchos;
prematureMarginL = this.prematureMarginL;
prematureMarginS = this.prematureMarginS;

first_echo_ind = [1:num_echo:4*num_echo]; % in the future, this should maybe only look at the overall peaks of the 1st echo: [1:num_echo:2*num_echo]

if wave_type == 'L'
    tof_margin = prematureMarginL;
elseif wave_type == 'S'
    tof_margin = prematureMarginS;
end

first_echo_times = res(first_echo_ind);

% Calculate average of ToFs that have been calculated so far
tof_average = mean(tof,2,'omitnan');

min_tof = tof_average*(1-tof_margin);
max_tof = tof_average*(1+tof_margin);

if any(first_echo_times < min_tof) || any(first_echo_times > max_tof)
    out = 1;
end

echo_prematurity = (first_echo_times - tof_average)/tof_average;

end
