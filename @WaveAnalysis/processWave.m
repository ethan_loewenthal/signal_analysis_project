function this = processWave(this)

data      = this.rawData;
wave_type = num2str(this.waveType);

if wave_type == 'L'
    num_echo = 3;
    tof_int  = this.groupWindowL;
elseif wave_type == 'S'
    num_echo = 2;
    tof_int  = this.groupWindowS;
else
    tof_int = nan;
end

this.numberofEchos = num_echo;
this.groupWindow   = tof_int;

% set up cell arrays & matrices to record results
loc_max_ind = cell(num_echo,1); % cell array holds time indices of all local maxs
[loc_max_ind{:}] = deal(nan);  % initialize all cells to nan
loc_min_ind = cell(num_echo,1); % cell array holds time indices of all local mins
[loc_min_ind{:}] = deal(nan);  % initialize all cells to nan

peak_to_noise_ratio = nan(1,2*num_echo); % pos and neg peak to noise ratios for each echo
echo_prematurity    = nan(1,4);          % record of the prematurity of each of the 4 peaks associated with the 1st echo
noise_pos    = nan(1,num_echo);       % record of the max positive noise associated with each echo
noise_neg    = nan(1,num_echo);       % record of the max negative noise associated with each echo
peak_sep     = nan(1,num_echo);       % time separation of max/min pair in each echo
res          = nan(1,4*num_echo);     % time associated with maxs & mins
echo_peaks   = nan(1,4*num_echo);     % amplitudes associated with maxs & mins
tof          = nan(1,4*(num_echo-1)); % ToFs
tof_sort     = nan(1,4*(num_echo-1)); % ToFs in ascending order
tof_weights  = nan(1,4*(num_echo-1)); % weights associated with ToFs in ascending order

this.localmaxIndices  = loc_max_ind;
this.localminIndices  = loc_min_ind;
this.peakToNoiseRatio = peak_to_noise_ratio;
this.echoPrematurity  = echo_prematurity;
this.signalNoisepos   = noise_pos;
this.signalNoiseneg   = noise_neg;
this.peakSeparation   = peak_sep;
this.peakTimes        = res;
this.peakAmplitudes   = echo_peaks;
this.timesofFlight    = tof;
this.tofAscending     = tof_sort;
this.tofWeights       = tof_weights;

conv = this.plottimeUnits/this.datatimeUnits; % calc time conversion parameter
data = data(:,end);
mn   = size(data);
time = (1:mn(1))'/conv; % make time axis (microseconds)
data(1:this.cleanBang) = 0; % clean the initial main bang (zero-out the 1st bunch of pts)

this.rawData = data;
this.Time    = time;

% Check for clipping, now that the initial "bang" has been zeroed out
this.clipped = checkClipping(this);
if this.clipped
    return
end
% ++++++++++++++++++---  Get 1st max & 1st min ---+++++++++++++++++++++
% get overall max and min
overall_max = max(data);
overall_min = min(data);

% find indices of all maxes and mins that are at least 50% of the overall
% max and min
max_ind = data > overall_max/2;
min_ind = data < overall_min/2;

% use indices of 1st max and 1st min as approximate indices of the 1st
% echo's 1st max and 1st min, and use their average
tm1_approx = find(max_ind~=0,1,'first');
tn1_approx = find(min_ind~=0,1,'first');
t1_approx  = min(tm1_approx,tn1_approx);

% set small time window around approximate ToF (1st echo), and
% get true max and min of first echo
fir          = 0*data;
t1_wind      = round((1-this.maxminWindow)*t1_approx):round((1+this.maxminWindow)*t1_approx);
fir(t1_wind) = data(t1_wind);
[firstp,tm1] = max(fir);
[firstv,tn1] = min(fir);

% +++++++++++++++++++---  Go thru each echo ---++++++++++++++++++++++++
for i=1:num_echo
    
    % +++++++++++++++++++---  Get peaks of each echo ---+++++++++++++++
    if i == 1
        tmi = tm1;
        tni = tn1;
        ip  = firstp;
        iv  = firstv;
    else
        % create window in which the echo is expected
        dip           = 0*data;
        din           = 0*data;
        tmi_wind      = round((i-this.maxminWindow)*tm1):round((i+this.maxminWindow)*tm1);
        tni_wind      = round((i-this.maxminWindow)*tn1):round((i+this.maxminWindow)*tn1);
        dip(tmi_wind) = data(tmi_wind);
        din(tni_wind) = data(tni_wind);
        
        % get the max & min and their indices from this echo
        [ip,tmi] = max(dip);
        [iv,tni] = min(din);
    end
    
    % convert to microseconds and record max & min of this echo
    res(1,i)                 = tmi/conv;
    res(1,i+num_echo)        = tni/conv;
    echo_peaks(1,i)          = ip;
    echo_peaks(1,i+num_echo) = iv;
    
    % calc ToFs
    if i > 1
        tof(1,i-1)              = res(1,i)          - res(1,i-1);
        tof(1,i-1+(num_echo-1)) = res(1,i+num_echo) - res(1,i+num_echo-1);
    end
    
    this.peakTimes     = res;
    this.timesofFlight = tof;
    
    % Check that all ToFs are in range (based on wave type and bolt
    % length) as soon as the 1st ToFs are known, and if not get out of this
    % function.
    if i > 1
        [this.actToExpectTOF, this.tofBad] = initialTofCheck(this);
        if this.tofBad
            return
        end
    end
    
    % Check for premature 1st echo as soon as the 1st ToFs are known
    if i == 2
        [this.echoPrematurity, this.boltFatigued] = fatigueCheck(this);
        if this.boltFatigued
            return
        end
    end
    
    % Check for min/max peak separations as each pair of echo peaks is
    % found
    [this.peakSeparation, this.maxminSep] = checkpeakSeparation(this);
    if this.maxminSep
        return
    end
    
    % +++++++--- Get local maxs and mins of this echo ---++++++
    if i == 1
        noise_window = this.cleanBang:round(tm1*(1-this.noiseWindow));
    else
        noise_window = round(tmi*(1-5*this.noiseWindow/i)):round(tmi*(1-this.noiseWindow/i));
    end
    
    noise        = data(noise_window);
    noiselevelpi = max(noise);
    noiselevelni = abs(min(noise));
    
    noise_pos(i) = noiselevelpi;
    noise_neg(i) = noiselevelni;
    
    this.signalNoisepos = noise_pos;
    this.signalNoiseneg = noise_neg;
    this.peakAmplitudes = echo_peaks;
    
    % Check for peak-to-noise ratios as noise for each echo peak is found
    [this.peakToNoiseRatio, this.noiseratioLow] = noiseRatioCheck(this);
    if this.noiseratioLow
        return
    end
    
    % Create window that spans from a point preceding the peak and then
    % ends at the peak
    windpi                    = 0*data;
    windni                    = 0*data;
    wind_size                 = round(tm1*this.locmaxminWindow);
    windpi(tmi-wind_size:tmi) = data(tmi-wind_size:tmi);
    windni(tni-wind_size:tni) = data(tni-wind_size:tni);
    
    % find local maxs and mins preceding the max and min of each echo
        phasepi = islocalmax(windpi,'MinProminence',...
            this.minProminence*noiselevelpi,'MinSeparation',this.locmaxminThreshold);
    
%     [~, phasepi] = findpeaks(windpi,'MinPeakProminence',...
%         this.minProminence*noiselevelpi,'MinPeakDistance',this.locmaxminThreshold);
%     phasepi = ismember(round(time*conv), phasepi);
    
        phaseni = islocalmin(windni,'MinProminence',...
            this.minProminence*noiselevelni,'MinSeparation',this.locmaxminThreshold);
%     
%     [~, phaseni] = findpeaks(-windni,'MinPeakProminence',...
%         this.minProminence*noiselevelpi,'MinPeakDistance',this.locmaxminThreshold);
%     phaseni = ismember(round(time*conv), phaseni);
    
    % eliminate local maxs and mins with amplitudes of the wrong sign
    local_max_ind                   = find(phasepi~=0);
    max_pos                         = find(data(phasepi)<0);
    phasepi(local_max_ind(max_pos)) = 0;
    
    local_min_ind                   = find(phaseni~=0);
    min_pos                         = find(data(phaseni)>0);
    phaseni(local_min_ind(min_pos)) = 0;
    
    loc_max_ind{i} = phasepi;
    loc_min_ind{i} = phaseni;
    
    % get times of local maxs & mins
    tofpi = time(phasepi);
    tofni = time(phaseni);
        
    % Remove the last local, which is the same as the biggest in the
    % echo (i.e. the peak). If there's only one local, leave as is.
    if numel(tofpi) > 1
        tofpi             = tofpi(1:end-1);
        last_ind          = find(phasepi==1,1,'last');
        phasepi(last_ind) = 0;
    else
        if this.localOption == 2 % if there are no locals, then there's no local before the peak: make it nan
            tofpi = nan;
        else
            tofpi = tmi/conv;
        end
    end
    
    if numel(tofni) > 1
        tofni             = tofni(1:end-1);
        last_ind          = find(phaseni==1,1,'last');
        phaseni(last_ind) = 0;
    else
        if this.localOption == 2 % if there are no locals, then there's no local before the peak: make it nan
            tofni = nan;
        else
            tofni = tni/conv;
        end
    end
    
    % record local maxs & mins of this echo
    if this.localOption == 1
        res(1,i+2*num_echo) = tofpi(1);
        res(1,i+3*num_echo) = tofni(1);
        
        pos_echo_peaks             = data(phasepi);
        neg_echo_peaks             = data(phaseni);
        echo_peaks(1,i+2*num_echo) = pos_echo_peaks(1);
        echo_peaks(1,i+3*num_echo) = neg_echo_peaks(1);
        
    elseif this.localOption == 2
        res(1,i+2*num_echo) = tofpi(end);
        res(1,i+3*num_echo) = tofni(end);
        
        pos_echo_peaks             = data(phasepi);
        neg_echo_peaks             = data(phaseni);
        echo_peaks(1,i+2*num_echo) = pos_echo_peaks(end);
        echo_peaks(1,i+3*num_echo) = neg_echo_peaks(end);
    end
    
    % calc ToFs
    if i > 1
        tof(1,i-1+2*(num_echo-1)) = res(1,i+2*num_echo) - res(1,i+2*num_echo-1);
        tof(1,i-1+3*(num_echo-1)) = res(1,i+3*num_echo) - res(1,i+3*num_echo-1) ;
    end
end

% tof_weights assigns 0 to overall echo peaks, 1 to in-phase peaks, and is
% ordered to match the ascending list of ToFs
[tof_sort, I] = sort(tof);
tof_weights   = [zeros(1,numel(tof)/2) ones(1,numel(tof)/2)];
tof_weights   = tof_weights(I);

this.rawData         = data;
this.Time            = time;
this.numberofEchos   = num_echo;
this.groupWindow     = tof_int;
this.signalNoisepos  = noise_pos;
this.signalNoiseneg  = noise_neg;
this.peakTimes       = res;
this.peakAmplitudes  = echo_peaks;
this.timesofFlight   = tof;
this.localmaxIndices = loc_max_ind;
this.localminIndices = loc_min_ind;
this.tofAscending    = tof_sort;
this.tofWeights      = tof_weights;

end