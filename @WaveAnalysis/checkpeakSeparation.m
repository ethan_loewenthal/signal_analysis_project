function [peak_sep, out] = checkpeakSeparation(this)

out = 0;

num_echos = this.numberofEchos;
res       = this.peakTimes;

peak_sep = res(1:num_echos) - res(num_echos+1:2*num_echos);

if any(abs(peak_sep) > this.maxpeakSep)
    out = 1;
end

end