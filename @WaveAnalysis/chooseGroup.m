function [best_group, group_weights, best_ind, out] = chooseGroup(this,prev_best_ind)

out = 0;

tof_sort         = this.tofAscending;
tof_weights_sort = this.tofWeights;
tof_groups       = this.tofGroups;
group_indices    = this.groupIndices;
weight_target_1  = this.groupweightTarget1;
weight_target_2  = this.groupweightTarget2;
tof_avg_sibling  = this.tofavgSibling;

% zero-out rows corresponding to previously picked groups
if ~isempty(prev_best_ind)
    group_indices(prev_best_ind,:) = 0;
end

num_groups     = size(tof_groups,1);
best_group_ind = [1:num_groups]; % initialize best_group_ind to include all possible groups

while true
    
    % Pick the group(s) with the greatest number of elements
    most_elements_ind = max(sum(group_indices,2)) == sum(group_indices,2);
    best_group_ind    = best_group_ind(most_elements_ind);
    
    if numel(best_group_ind) == 1
        break
    end
    
    % If there's still more than one group, pick the group with the
    % smallest spread of ToF's
    spreads = max(tof_groups(best_group_ind,:),[],2) - min(tof_groups(best_group_ind,:),[],2);
    smallest_spread_ind = min(spreads) == spreads;
    best_group_ind = best_group_ind(smallest_spread_ind);
    
    if numel(best_group_ind) == 1
        break
    end
    
    % If there's still more than one group, pick the one with the average
    % weight closest to the 1st group weight target. And, if there's still
    % more than one, use the 2nd group weight target.
    for weight_target = [weight_target_1 weight_target_2]
        
        tof_weights = nan*group_indices(best_group_ind,:);
        tof_weights_sort_mat = tof_weights_sort(ones(numel(best_group_ind),1),:);
        tof_weights(group_indices(best_group_ind,:) == 1) =...
            tof_weights_sort_mat(group_indices(best_group_ind,:) == 1);
        group_weight_mean = mean(tof_weights,2,'omitnan');
        
        group_weight_mean_diff     = abs(group_weight_mean - weight_target);
        group_weight_mean_diff_ind = min(group_weight_mean_diff) == group_weight_mean_diff;
        best_group_ind             = best_group_ind(group_weight_mean_diff_ind);
        
        if numel(best_group_ind) == 1
            break
        end
    end
    
    if numel(best_group_ind) == 1
        break
    end
    
    % If there's still more than one group, compare the averages to the
    % average ToF of this tension-load pair (sibling) to break the tie. If
    % the average ToF(s) of a sibling signal is not available, skip this
    % step
    if ~isempty(tof_avg_sibling) && ~isnan(tof_avg_sibling) 
        
        group_tof_mean      = mean(tof_groups(best_group_ind,:),2,'omitnan');
        group_mean_diff     = abs(group_tof_mean - tof_avg_sibling);
        group_mean_diff_ind = min(group_mean_diff) == group_mean_diff;
        best_group_ind      = best_group_ind(group_mean_diff_ind);
        
    end
        
    if numel(best_group_ind) == 1
        break
    end
    
    
    % If there's still more than one group, arbitrarily pick the first
    % group and tag this analysis as having had a group-choosing tie.
    out = 1;
    best_group_ind = best_group_ind(1);
    
    if numel(best_group_ind) == 1
        break
    end
end

best_group_indices = group_indices(best_group_ind,:);
best_group         = tof_sort(best_group_indices == 1);
group_weights      = tof_weights_sort(best_group_indices == 1);
best_ind           = [prev_best_ind best_group_ind];

end