function [best_group, actual_to_expected_tof_diff, out] = tofCheck(this)

out = 0;

first_group  = this.firsttofGroup;
second_group = this.secondtofGroup;
bolt_length  = this.boltLength;
wave_type    = num2str(this.waveType);
wave_speed_L = this.wavespeedL;
wave_speed_S = this.wavespeedS;
tof_margin_L = this.tofMarginL;
tof_margin_S = this.tofMarginS;

if wave_type == 'L'
    wave_speed = wave_speed_L;
    tof_margin = tof_margin_L;
elseif wave_type == 'S'
    wave_speed = wave_speed_S;
    tof_margin = tof_margin_S;
end

expected_time     =  2*bolt_length/wave_speed;
min_expected_time = (2*bolt_length/wave_speed)*(1-tof_margin);
max_expected_time = (2*bolt_length/wave_speed)*(1+tof_margin);

best_group  = first_group;
tof_average = mean(best_group);

if tof_average < min_expected_time || tof_average > max_expected_time
    best_group = second_group;
    tof_average = mean(best_group);
    if tof_average < min_expected_time || tof_average > max_expected_time
        best_group(:) = nan;
        tof_average = nan;
        out = 1;
    end
end

actual_to_expected_tof_diff = (tof_average - expected_time)/expected_time;

end