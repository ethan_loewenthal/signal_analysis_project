classdef WaveAnalysis
    
    % This class requires a bolt length and analyzes one signal and
    % produces an object that contains times & amplitudes associated with
    % maxs & mins (overall & in-phase), times of flight (ToFs), ToF groups,
    % average ToF, etc.
    %
    % analyzeWaves.m is the function that iterates this class to produce
    % multiple objects, one for each data file, and then runs WaveGroup.m
    % to pair signals and calculate UT responses
    
    properties % (SetAccess = private)
        
        % +++++++++++++++--- Likely to Require Adjustment ---++++++++++++++
        maxminWindow       = 0.07; % multiple of approx TOF, used to determine overall echo maxs & mins
        prematureMarginL   = 0.02; % multiple of avg L wave ToF; used to check if 1st echo is premature
        prematureMarginS   = 0.02; % multiple of avg S wave ToF; used to check if 1st echo is premature
        tofMarginL         = 0.15; % multiple of expected L wave ToF, based on bolt length; used to check that avg ToF is in expected range
        tofMarginS         = 0.15; % multiple of expected S wave ToF, based on bolt length; used to check that avg ToF is in expected range
        minnoiseRatio      = 2.5;  % minimum value for overall-echo-peak-to-noise ratio
        maxpeakSep         = 6;    % maximum # of microseconds separation from 1st max/min in each echo to the other min/max in that echo
        
        % +++++++++++++--- Not Likely to Require Adjustment ---++++++++++++
        clippingAmp        = 0.5;  % max allowable signal amplitude, defines clipping threshold
        cleanBang          = 5000; % first # of data pts to be zeroed-out, 5000 recommended for both S and L; previously used '2000' for L, '5000' for S
        localOption        = 1;    % '1' to pick 1st in-phase max & min, '2' to pick the last one before the overall peak of a given echo
        locmaxminWindow    = 0.05; % multiple of approx TOF, used to determine in-phase maxs & mins
        noiseWindow        = 0.05; % multiple of approx TOF, used to determine noise
        minProminence      = 2.2;  % multiple of max noise, used for minimum prominence in finding in-phase maxs & mins
        locmaxminThreshold = 10;   % min separation, used in finding in-phase maxs & mins
        groupWindowL       = 0.2;  % # of microseconds in interval used to group L wave ToFs
        groupWindowS       = 0.4;  % # of microseconds in interval used to group S wave ToFs
        groupweightTarget1 = 0.5;  % 1st priority for the most-highly valued group weight average; echo peaks: 0, in-phase: 1, even mix: 0.5
        groupweightTarget2 = 0;    % 2nd priority for the most-highly valued group weight average; echo peaks: 0, in-phase: 1, even mix: 0.5
        datatimeUnits      = 1e-8; % time between each data pt (s)
        plottimeUnits      = 1e-6; % time units used in plots (s)
        wavespeedL         = 5.94000082; % longitudinal wave speed in steel in mm/microsecond (0.2338583 in/microsecond)
        wavespeedS         = 3.22000118; % shear wave speed in steel in mm/microsecond(0.1267717 in/microsecond)
        
        rawData         % Array of doubles - raw data
        Time            % Array of doubles - raw data
        waveType        % L or S
        numberofEchos   % # of echos from which data is taken; code chooses 3 for L, 2 for S,
        groupIndices    % index matrix of all unique ToF groups; each row specifies a unique group
        groupWindow     % # of microseconds in interval used to group ToFs to be averaged
        boltTension     % bolt tension
        boltLength      % bolt length in mm, used to check ToF range
        sigNumber       % designates the number of the signal for a given tension value
        peakTimes       % times associated with maxs & mins
        peakAmplitudes  % amplitudes of maxs & mins
        localmaxIndices % time indices of in-phase maxs
        localminIndices % time indices of in-phase mins
        timesofFlight   % times of flight (ToFs)
        tofWeights      % array of values corresponding to types of peakTimes used to calculate ToF's: 0 for overall echo maxs & mins, 1 for in-phase values
        tofAscending    = nan; % ToFs sorted in ascending order
        tofGroups       % matrix of ToF groups
        groupWeights1   % weights associated with the 1st ToF group
        groupWeights2   % weights associated with the 2nd ToF group
        firsttofGroup   = nan; % 1st ToF group
        secondtofGroup  = nan; % 2nd ToF group
        besttofGroup    = nan; % best ToF group after tofCheck
        averageTof      = nan; % average ToF (mean of the best group)
        signalNoisepos  % maximum positive signal noise
        signalNoiseneg  % maximum negative signal noise
        tofavgSibling   % ToF average from sibling signal (used to break group-choosing tie)
        peakToNoiseRatio % pos & neg peak-to-noise ratios of each echo
        actToExpectTOF  = nan; % ratio of actual ToFs compared to expected, based on bolt length
        echoPrematurity % prematurity of echo peaks, divided by average ToF
        peakSeparation  % separation of 1st max or min in each echo to the 2nd in that echo, divided by average ToF
        
        % checks
        status	      = 0;     % Boolean - 0 if good, 1 if bad
        clipped       = false; % false if OK, true if signal clipped
        groupTie      = false; % false if OK, true if chooseGroup failed to find a unique best group
        noiseratioLow = false; % false if OK, true if ratio of peak-to-noise fails to exceed minimum
        tofBad        = false; % false if OK, true if average ToF does not fall in expected range, based on bolt length OR if 1st echo is premature
        boltFatigued  = false; % false if OK, true if 1st echo is premature; bolt labeled as fatigued
        maxminSep     = false; % false if OK, true if separation from 1st max or min in each echo to the 2nd in that echo is too large (this may be unnecessary; grouping may take care of this)
        isNan         = false; % false if OK, true if the output is nan
        emptyData     = false; % false if OK, true if data set is empty
        programCrash  = false; % false if OK, true if there was a crash
        
    end
    
    methods
        
        function this = WaveAnalysis(data,bolt_length,wave_type,tension,sig_num,tof_avg_sibling)
            
            this.rawData       = data;
            this.boltLength    = bolt_length;
            this.waveType      = wave_type;
            this.boltTension   = tension;
            this.sigNumber     = sig_num;
            this.tofavgSibling = tof_avg_sibling;
            
            try
                this = processWave(this); % process the signal to get maxs & mins, ToFs, etc.
            catch
                % throw an error that the wave processing has failed, set
                % status and programCrash to 1, and get out of this
                % function: WaveAnalysis
                this.programCrash = 1;
                this.status = 1;
                return
            end
            
            this.tofBad = any([this.tofBad this.boltFatigued]); % consistent with json output, declare ToF bad if the 1st echo is premature
            
            % If any of the checks inside processWave failed, get out of
            % this function: WaveAnalysis.
            if any([this.clipped, this.tofBad, this.boltFatigued, this.maxminSep, this.noiseratioLow])
                this.status = 1;
                return
            end
            
            this = defineGroups(this); % get group indices and the groups themselves
            [this.firsttofGroup, this.groupWeights1, best_ind, out2a] = chooseGroup(this,[]); % choose 1st group
            [this.secondtofGroup, this.groupWeights2, ~,       out2b] = chooseGroup(this,best_ind); % choose 2nd group
            this.groupTie = any([out2a,out2b] == 1);
            [this.besttofGroup, this.actToExpectTOF, this.tofBad] = tofCheck(this); % check that the average ToF falls in the expected range, based on bolt length; use the 2nd group if the 1st group's avg fails the check, none if neither pass the check
            this.averageTof = mean(this.besttofGroup); % get average ToF

            this.isNan     = isnan(this.averageTof);
            this.emptyData = isempty(this.averageTof);

            % define status (omit groupTie because a group tie isn't really an "error")
            if any([this.tofBad, this.isNan, this.emptyData] == 1)
                this.status = 1;
            end
        end
    end
end