function [utObj, waveObjs] = researchDriver(folder,boltLength)

% This function analyzes waves for the research team. It includes functions
% that plot the raw signal with maxs & mins marked, print information in
% the command window about each signal, and print failed check results
% when one or more checks fail. It also produces csv files so the output
% can be visually validated.

for ifolder = 1:length(folder)
    thisfolder = folder{ifolder};
    
    % files     = dir(fullfile(folder,'*.txt'));
    files     = dir(fullfile(thisfolder,'*.txt'));
    num_sigs  = length(files);
    data      = cell(num_sigs,1);
    wave_type = cell(num_sigs,1);
    tension   = nan(num_sigs,1);
    sig_num   = nan(num_sigs,1);
    
    for i = 1:num_sigs
        
        filename     = fullfile(thisfolder,files(i).name);
        data{i}      = load(filename); % load data
        filename     = files(i).name;

        % get wave type
        name         = string(filename);
        name         = split(name,'.');
        name         = split(name(1));
        wave_type{i} = name(4);
        
        % get tension and signal number
        [num,tf] = str2num(name(6));
        if tf == 1
            tension(i) = num;
            sig_num(i) = 1;
        else
            name     = split(name(6),'(');
            [num,tf] = str2num(name(1));
            tension(i)  = num;
            name     = split(name(2),')');
            [num,tf] = str2num(name(1));
            sig_num(i)  = num+1;
        end
    end
    
    [~,utObj,waveObjs] = analyzeWaves(data,boltLength,wave_type,tension,sig_num);
    
    for i = 1:num_sigs
        
        printInfo(waveObjs(i)); % print analysis info for each signal
%         signalErrorPrint(waveObjs(i)) % print what data checks fail
        
        if ~waveObjs(i).programCrash
            signalPlot(waveObjs(i)); % plot each signal, with maxs & mins marked & labeled; don't plot signal if bolt is fatigued or processWave crashed
        else
            errordlg('Signal analyzer was unable to extract data from the signal for tension of ', num2str(waveObjs(i).boltTension), ', signal # ', num2str(waveObjs(i).sigNumber),'. Data will not be plotted.');
        end
    end
    
%     outputErrorPrint(utObj); % print what output checks fail
    printOutput(utObj,folder{ifolder});      % print results to csv files for validation purposes
    
end

end
