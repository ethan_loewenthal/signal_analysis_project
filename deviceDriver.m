function jsonOutput = deviceDriver(L_data,S_data,boltLength)

% get # of signals
num_sigs_L = size(L_data,1);
num_sigs_S = size(S_data,1);
num_sigs   = num_sigs_L + num_sigs_S;

% hardcode tension and sig_num as unity (just placeholders, for now)
tension   = ones(num_sigs,1);
sig_num   = ones(num_sigs,1);

data      = cell(num_sigs,1);
wave_type = cell(num_sigs,1);

for i = 1:num_sigs_L
    data{i} = L_data{i}; % load data
    wave_type{i} = 'L';
end

for i = 1:num_sigs_S
    data{num_sigs_L+i} = S_data{i}; % load data
    wave_type{num_sigs_L+i} = 'S';
end

[jsonOutput,~,~] = analyzeWaves(data,boltLength,wave_type,tension,sig_num);

end