function [L1 L2 S1 S2] = convertDataSample(L_data, S_data)

% Input is in the form of two cell arrays, one for L and one for S. For
% now, I'm writing this function with the assumption there are the same
% number of signals in each array, and that number is 2, just to make sure
% we have our indexing right. The data in each cell can be any length, as
% long as it is only one column (e.g. 60,000 rows x 1 column or 60,007 rows
% x 1 column).

% The following is included to test the code and to show the format of the
% arrays. To avoid this, comment out everything down to 'function starts
% here'.
% 
% L_data    = cell(2,1);
% S_data    = cell(2,1);
% 
% L_data{1} = [1:10]';
% L_data{2} = [11:19]';
% 
% S_data{1} = [10:10:100]';
% S_data{2} = [110:10:170]';

%+++++++++++++++++++--- function starts here ---+++++++++++++++++++++++++++
L1 = L_data{1};
L2 = L_data{2};
S1 = S_data{1};
S2 = S_data{2};

end