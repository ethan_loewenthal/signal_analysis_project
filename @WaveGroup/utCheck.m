function out = utCheck(this)

out = 0;

min_ut      = this.utMin;
max_ut      = this.utMax;
ut_response = this.avgutResponse;

if any(ut_response < min_ut) || any(ut_response > max_ut)
    out = 1;
end

end