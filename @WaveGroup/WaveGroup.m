classdef WaveGroup
    
    % This class processes ToFs from multiple wave signals and produces UT
    % responses, checks that those values fall into the expected window, and
    % then averages the results
    
    properties % (SetAccess = private)
        
        utMin = 1.804309868; % min acceptable UT response
        utMax = 1.841133997; % max acceptable UT response
        
        avgtofL          % average ToF of all L signals
        avgtofS          % average ToF of all S signals
        avgutResponse    % average UT response for each tension
        utResponse       % UT response (UNUSED)
        signalProperties % all properties from each signal (UNUSED)
        
        validationL      % L wave results to be printed to csv in printOutput function
        validationS      % S wave results to be printed to csv in printOutput function
        validationUt     % UT response results to be printed to csv in printOutput function
        validationUtavg  % average UT response results for each tension to be printed to csv in printOutput function
        
        % checks
        status	     = 0;     % Boolean - 0 if good, 1 if bad
        signalStatus = 0;     % 0 if OK, 1 if status of ANY signal is bad (Note: this is NOT used to set group status, for now)
        utBad        = false; % false if OK, true if UT response(s) is not in acceptable range
        emptyData    = false; % false if OK, true if the output is empty
        isNan        = false; % false if OK, true if the output is nan
        
    end
    
    methods
        
        function this = WaveGroup(waveObjs,num_sigs)
            
            bolt_tension           = nan(num_sigs,1);
            sig_num                = nan(num_sigs,1);
            avg_tofs               = nan(num_sigs,1);
            tofs                   = nan(num_sigs,8);
            tof_sort               = nan(num_sigs,8);
            first_group            = nan(num_sigs,8);
            second_group           = nan(num_sigs,8);
            best_group             = nan(num_sigs,8);
            peak_to_noise_ratio    = nan(num_sigs,6);
            actual_to_expected_tof = nan(num_sigs,1);
            echo_prematurity       = nan(num_sigs,4);
            peak_sep               = nan(num_sigs,3);
            status_	               = nan(num_sigs,1);
            clipped_               = nan(num_sigs,1);
            group_tie              = nan(num_sigs,1);
            noise_ratio_low        = nan(num_sigs,1);
            tof_bad                = nan(num_sigs,1);
            bolt_fatigued          = nan(num_sigs,1);
            max_min_sep            = nan(num_sigs,1);
            is_nan                 = nan(num_sigs,1);
            empty_data             = nan(num_sigs,1);
            wave_type              = strings(num_sigs,1);
            program_crash          = nan(num_sigs,1);
            
            for i = 1:num_sigs
                
                wave_type(i)    = num2str(waveObjs(i).waveType);
                bolt_tension(i) = waveObjs(i).boltTension;
                sig_num(i)      = waveObjs(i).sigNumber;
                avg_tofs(i)     = waveObjs(i).averageTof;
                
                peak_to_noise_ratio(i,1:numel(waveObjs(i).peakToNoiseRatio)) = waveObjs(i).peakToNoiseRatio;
                actual_to_expected_tof(i)                                    = waveObjs(i).actToExpectTOF;
                echo_prematurity(i,1:numel(waveObjs(i).echoPrematurity))     = waveObjs(i).echoPrematurity;
                peak_sep(i,1:numel(waveObjs(i).peakSeparation))              = waveObjs(i).peakSeparation;
                
                tofs(i,1:numel(waveObjs(i).timesofFlight))          = waveObjs(i).timesofFlight;
                tof_sort(i,1:numel(waveObjs(i).tofAscending))       = waveObjs(i).tofAscending;
                first_group(i,1:numel(waveObjs(i).firsttofGroup))   = waveObjs(i).firsttofGroup;
                second_group(i,1:numel(waveObjs(i).secondtofGroup)) = waveObjs(i).secondtofGroup;
                best_group(i,1:numel(waveObjs(i).besttofGroup))     = waveObjs(i).besttofGroup;
                
                status_(i)	       = waveObjs(i).status;
                clipped_(i)        = waveObjs(i).clipped;
                group_tie(i)       = waveObjs(i).groupTie;
                noise_ratio_low(i) = waveObjs(i).noiseratioLow;
                tof_bad(i)         = waveObjs(i).tofBad;
                bolt_fatigued(i)   = waveObjs(i).boltFatigued;
                max_min_sep(i)     = waveObjs(i).maxminSep;
                is_nan(i)          = waveObjs(i).isNan;
                empty_data(i)      = waveObjs(i).emptyData;
                program_crash(i)   = waveObjs(i).programCrash;
                
            end
            
            ind_L = wave_type == 'L';
            ind_S = wave_type == 'S';
            
            tension_L      = bolt_tension(ind_L);
            sig_num_L      = sig_num(ind_L);
            tofs_L         = tofs(ind_L,:);
            tof_sort_L     = tof_sort(ind_L,:);
            first_group_L  = first_group(ind_L,:);
            second_group_L = second_group(ind_L,:);
            best_group_L   = best_group(ind_L,:);
            avg_tofs_L     = avg_tofs(ind_L);
            peak_to_noise_ratio_L    = peak_to_noise_ratio(ind_L,:);
            actual_to_expected_tof_L = actual_to_expected_tof(ind_L,:);
            echo_prematurity_L       = echo_prematurity(ind_L,:);
            peak_sep_L               = peak_sep(ind_L,:);
            
            status_L	      = status_(ind_L);
            clipped_L         = clipped_(ind_L);
            group_tie_L       = group_tie(ind_L);
            noise_ratio_low_L = noise_ratio_low(ind_L);
            tof_bad_L         = tof_bad(ind_L);
            bolt_fatigued_L   = bolt_fatigued(ind_L);
            max_min_sep_L     = max_min_sep(ind_L);
            is_nan_L          = is_nan(ind_L);
            empty_data_L      = empty_data(ind_L);
            program_crash_L   = program_crash(ind_L);
            
            nan_col_L      = nan(numel(avg_tofs_L),1);
            
            tension_S      = bolt_tension(ind_S);
            sig_num_S      = sig_num(ind_S);
            tofs_S         = tofs(ind_S,:);
            tof_sort_S     = tof_sort(ind_S,:);
            first_group_S  = first_group(ind_S,:);
            second_group_S = second_group(ind_S,:);
            best_group_S   = best_group(ind_S,:);
            avg_tofs_S     = avg_tofs(ind_S);
            peak_to_noise_ratio_S    = peak_to_noise_ratio(ind_S,:);
            actual_to_expected_tof_S = actual_to_expected_tof(ind_S,:);
            echo_prematurity_S       = echo_prematurity(ind_S,:);
            peak_sep_S               = peak_sep(ind_S,:);
            
            status_S	      = status_(ind_S);
            clipped_S         = clipped_(ind_S);
            group_tie_S       = group_tie(ind_S);
            noise_ratio_low_S = noise_ratio_low(ind_S);
            tof_bad_S         = tof_bad(ind_S);
            bolt_fatigued_S   = bolt_fatigued(ind_S);
            max_min_sep_S     = max_min_sep(ind_S);
            is_nan_S          = is_nan(ind_S);
            empty_data_S      = empty_data(ind_S);
            program_crash_S   = program_crash(ind_S);
            
            nan_col_S      = nan(numel(avg_tofs_S),1);
            
            resultsL = [tension_L sig_num_L avg_tofs_L nan_col_L tofs_L nan_col_L...
                tof_sort_L nan_col_L first_group_L nan_col_L second_group_L...
                nan_col_L best_group_L nan_col_L peak_to_noise_ratio_L nan_col_L...
                actual_to_expected_tof_L nan_col_L echo_prematurity_L nan_col_L...
                peak_sep_L nan_col_L status_L clipped_L group_tie_L noise_ratio_low_L...
                tof_bad_L bolt_fatigued_L max_min_sep_L is_nan_L empty_data_L program_crash_L];
            
            resultsS = [tension_S sig_num_S avg_tofs_S nan_col_S tofs_S nan_col_S...
                tof_sort_S nan_col_S first_group_S nan_col_S second_group_S...
                nan_col_S best_group_S nan_col_S peak_to_noise_ratio_S nan_col_S...
                actual_to_expected_tof_S nan_col_S echo_prematurity_S nan_col_S...
                peak_sep_S nan_col_S status_S clipped_S group_tie_S noise_ratio_low_S...
                tof_bad_S bolt_fatigued_S max_min_sep_S is_nan_S empty_data_S program_crash_S];
            
            resultsL = sortrows(resultsL);
            resultsS = sortrows(resultsS);
            
            % eliminate data from runs that are not common to both L and S wave data
            if any(isnan(resultsL(:,1))) || any(isnan(resultsS(:,1)))
                comm_col = 2;
            else
                comm_col = [1:2];
            end
            
            comm_ind      = ismember(resultsS(:,comm_col),resultsL(:,comm_col),'rows');
            ResultsS_comm = resultsS(comm_ind,1:3);
            comm_ind      = ismember(resultsL(:,comm_col),ResultsS_comm(:,comm_col),'rows');
            ResultsL_comm = resultsL(comm_ind,1:3);
            
            % calc UT responses if S & L signals can be matched
            if size(ResultsS_comm,1) == size(ResultsL_comm,1)
                avg_tofs_L  = ResultsL_comm(:,3);
                avg_tofs_S  = ResultsS_comm(:,3);
                ut_response = avg_tofs_S./avg_tofs_L;
                ten_ut      = [ResultsL_comm(:,1:2) ut_response];
                
                % average UT responses of recurring tension values
                [unique_tension_values, ~, ic] = unique(ten_ut(:,1));
                
                % ut_response_average = nan(numel(unique_tension_values),1);
                average_tof_L = nan(numel(unique_tension_values),1);
                average_tof_S = nan(numel(unique_tension_values),1);
                
                for i = 1:numel(unique_tension_values)
                    average_tof_L(i) = mean(avg_tofs_L(ic == i,end));
                    average_tof_S(i) = mean(avg_tofs_S(ic == i,end));
                end
                
                ut_response_average = average_tof_S./average_tof_L;
                
                ten_ut_avg = [unique_tension_values ut_response_average];
            else
                
                ten_ut = nan;
                ten_ut_avg = nan;
            end
            
            this.avgtofL         = average_tof_L;
            this.avgtofS         = average_tof_S;
            this.avgutResponse   = ut_response_average;
            this.validationL     = resultsL;   % L wave results to be printed to csv in printOutput function
            this.validationS     = resultsS;   % S wave results to be printed to csv in printOutput function
            this.validationUt    = ten_ut;
            this.validationUtavg = ten_ut_avg;
            
            out1 = utCheck(this);
            this.utBad = out1;
            
            out2a = any(isnan(average_tof_L));
            out2b = any(isnan(average_tof_S));
            out2c = any(isnan(ut_response_average));
            out2  = any([out2a, out2b, out2c] == 1);
            this.isNan = out2;
            
            out3a = any(isempty(average_tof_L));
            out3b = any(isempty(average_tof_S));
            out3c = any(isempty(ut_response_average));
            out3  = any([out3a, out3b, out3c] == 1);
            this.emptyData = out3;
            
            % set group status
            if any([out1, out2, out3] == 1)
                this.status = 1;
            end
        end
    end
end