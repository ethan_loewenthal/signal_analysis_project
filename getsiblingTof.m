function tof_avg_sibling = getsiblingTof(waveObjs,num_sigs,j)

sibling_tofs = [];
signals      = [1:num_sigs];
signals      = signals(signals~=j); % only look at signals other than the one that has the tie

for i = signals
    if waveObjs(i).waveType == waveObjs(j).waveType &&...
            waveObjs(i).boltTension == waveObjs(j).boltTension
        sibling_tofs = [sibling_tofs waveObjs(i).averageTof];
    end
end

tof_avg_sibling = mean(sibling_tofs);

end