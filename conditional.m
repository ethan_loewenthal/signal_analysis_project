function c = conditional(condition , istrue , isfalse)
    if condition
        c = istrue;
    else
        c = isfalse;
    end
end