function [L_data, S_data] = getData(folder)

num_sigs_picked = 20;

files     = dir(fullfile(folder,'*.txt'));
num_files = length(files);
data      = cell(num_files,1);
L_data    = cell(num_sigs_picked,1);
S_data    = cell(num_sigs_picked,1);

for i = 1:num_files
    
    filename = files(i).name;
    data{i}  = load(filename); % load data
    
end

for i = 1:num_sigs_picked
    L_data{i} = data{i};
    S_data{i} = data{end-(num_sigs_picked-i)};
end
end