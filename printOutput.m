function printOutput(utObj,folder)

ten_ut     = utObj.validationUt;    % UT response (with tension values and run number)
ten_ut_avg = utObj.validationUtavg; % average UT response for each tension (with tension values)
resultsL   = utObj.validationL;     % L wave results to be printed to csv
resultsS   = utObj.validationS;     % S wave results to be printed to csv

% write tension and UT response data to csv file
writematrix(ten_ut,fullfile(folder,'UT.csv'));
writematrix(ten_ut_avg,fullfile(folder,'UT_avg.csv'));
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% write extended results to csv file (use for validaton only)
str1  = "tension";
str2  = "signal #";
str3  = "average ToF";
str4  = "max to max";
str5  = "min to min";
str6  = " ";
str7  = "echo peak";
str8  = "initial in phase";
str9  = string(strcat(['x' ' ' num2str(1e-6) ' ' 's']));
str10 = "ordered ToFs";
str11 = "1st group --->";
str12 = "2nd group --->";
str13 = "best group --->";
str14 = "peak-to-noise ratio";
str15 = "((avg ToF - expected ToF) / expected ToF) ; expected calculated using bolt length";
str16 = "1st echo prematurity: (1st echo time - avg ToF) / avg ToF";
str17 = "pos peak - neg peak separation, 1 for each echo (microseconds)";
str18 = ["status" "clipped" "group tie" "noise ratio low" "tof bad" "bolt fatigued" "max/min separation" "is nan" "empty data" "program crash"];

for wave_type = ['L' 'S']
    
    if wave_type == 'L'
        num_echo = 3;
    elseif wave_type == 'S'
        num_echo = 2;
    end
    
    heading1(1)                                   = str1;
    heading1(2)                                   = str2;
    heading1(3)                                   = str3;
    heading1(4)                                   = str6;
    heading1(4+0*(num_echo-1)+1:4+1*(num_echo-1)) = str4;
    heading1(4+1*(num_echo-1)+1:4+2*(num_echo-1)) = str5;
    heading1(4+2*(num_echo-1)+1:4+3*(num_echo-1)) = str4;
    heading1(4+3*(num_echo-1)+1:4+4*(num_echo-1)) = str5;
    
    heading2(1:4)                                 = str6;
    heading2(4+0*(num_echo-1)+1:4+2*(num_echo-1)) = str7;
    heading2(4+2*(num_echo-1)+1:4+4*(num_echo-1)) = str8;
    
    heading3(1:4)                                 = str6;
    heading3(4+0*(num_echo-1)+1:4+4*(num_echo-1)) = str9;
    
    heading4(1:4*(num_echo-1))                    = str6;
    
    heading5(1)                                   = str10;
    heading5(2:4*(num_echo-1))                    = str6;
    
    heading6(1)                                   = str11;
    heading6(2:4*(num_echo-1))                    = str6;
    
    heading7(1)                                   = str12;
    heading7(2:4*(num_echo-1))                    = str6;
    
    heading8(1)                                   = str13;
    heading8(2:4*(num_echo-1))                    = str6;
    
    heading9(1:4*(num_echo-1))                    = str6;
    
    heading10(1)                                  = str14;
    heading10(2:2*num_echo)                       = str6;

    heading11(1)                                  = str15;
    
    heading12(1)                                  = str16;
    heading12(2:4)                                = str6;
    
    heading13(1)                                  = str17;
    heading13(2:num_echo)                         = str6;
    
    heading14(1:num_echo)                         = str6;

    heading15(1)                                  = str6;

    heading16(1:2*num_echo)                       = str6;

    heading17(1:4)                                = str6;

    heading18(1:10)                                = str18;
    
    heading19(1:10)                                = str6;
    
    if wave_type == 'L'
        
        resultsL_plot = [heading1 nan heading4 nan heading4 nan heading4 nan heading4 nan heading16 nan heading15 nan heading17 nan heading14 nan heading19;...
                         heading2 nan heading4 nan heading4 nan heading4 nan heading4 nan heading16 nan heading15 nan heading17 nan heading14 nan heading19;...
                         heading3 nan heading5 nan heading6 nan heading7 nan heading8 nan heading10 nan heading11 nan heading12 nan heading13 nan heading18;...
                         resultsL];
        
        writematrix(resultsL_plot,fullfile(folder,'TOFL_ext.csv'));
        
    elseif wave_type == 'S'
        
        resultsS_plot = [heading1 heading9 nan heading4 heading9 nan heading4 heading9 nan heading4 heading9 nan heading4 heading9 nan heading16 heading14 nan heading15 nan heading17 nan heading14 heading15 nan heading19;...
                         heading2 heading9 nan heading4 heading9 nan heading4 heading9 nan heading4 heading9 nan heading4 heading9 nan heading16 heading14 nan heading15 nan heading17 nan heading14 heading15 nan heading19;...
                         heading3 heading9 nan heading5 heading9 nan heading6 heading9 nan heading7 heading9 nan heading8 heading9 nan heading10 heading14 nan heading11 nan heading12 nan heading13 heading15 nan heading18;...
                         resultsS];
        
        writematrix(resultsS_plot,fullfile(folder,'TOFS_ext.csv'));
    end
    
    clear heading1;
    clear heading2;
    clear heading3;
    clear heading4;
    clear heading5;
    clear heading6;
    clear heading7;
    clear heading8;
    clear heading9;
    clear heading10;
    clear heading11;
    clear heading12;
    clear heading13;
    clear heading14;
    clear heading15;
    clear heading16;
    clear heading17;

end
end