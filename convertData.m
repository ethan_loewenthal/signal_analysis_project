function data = convertData(data,filename)

if isempty(data) && ~isempty(filename)
    data = load(filename); % load data
end
end