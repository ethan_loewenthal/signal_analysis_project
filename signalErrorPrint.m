function signalErrorPrint(this)

if this.status == 1
    
    fprintf('one or more checks of the signal or results failed \n');
    
    clipped_       = this.clipped
    groupTie_      = this.groupTie
    noiseratioLow_ = this.noiseratioLow
    tofBad_        = this.tofBad
    boltFatigued_  = this.boltFatigued
    maxminSep_     = this.maxminSep
    isNan_         = this.isNan
    emptyData_     = this.emptyData
    
end