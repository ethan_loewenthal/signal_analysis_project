function strJson = deviceDriverAlt(L_data,S_data,boltLength)

% get # of signals
num_sigs_L = size(L_data,1);
num_sigs_S = size(S_data,1);
num_sigs   = num_sigs_L + num_sigs_S;

sProps_group = struct('num_sigs', num_sigs, ...
    'num_sigs_L',   num_sigs_L, ...
    'num_sigs_S',         num_sigs_S);%...

strJson = jsonencode(sProps_group);

end