function printInfo(waveObjs)

num_echo    = waveObjs.numberofEchos;
tension     = waveObjs.boltTension;
sig_num     = waveObjs.sigNumber;
tof_average = waveObjs.averageTof;
group_tie   = waveObjs.groupTie;

if num_echo == 3
    fprintf(['     Analyzing longitudinal wave data, so the number of echos is ', num2str(num_echo), '\n']);
elseif num_echo == 2
    fprintf(['     Analyzing shear wave data, so the number of echos is ', num2str(num_echo), '\n']);
else
    fprintf(['     Analyzing unknown wave type, so leaving the number of echos as is:', num2str(num_echo), '\n']);
end

fprintf(['          Tension on this bolt is ', num2str(tension), ', signal # ', num2str(sig_num), ' \n']);
fprintf(['               ...and the average time of flight is ', num2str(tof_average), ' microseconds \n']);

if group_tie == 1
    fprintf(['chooseGroup came up with a tie and is unable to determine the best group. The average ToF of other analyses at the same tension will be used to break the tie \n']);
end
end